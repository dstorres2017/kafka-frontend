import { SeccionService } from './../../services/seccion.service';
import { Component, OnInit, ViewChild, ElementRef, ViewChildren } from '@angular/core';
import { Genero } from '../../core/modules/genero.model';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LibroService } from 'src/app/services/libro.service';
import { Router } from '@angular/router';

import { Response } from '../../core/modules/response.model';
import { EscritorService } from '../../core/services/escritor.service';
import { HttpErrorResponse } from '@angular/common/http';
import { interval } from 'rxjs';
import * as WS from '../../core/properties/configuracion.parameter';


@Component({
  selector: 'app-formlibro',
  templateUrl: './formlibro.component.html',
  styleUrls: ['./formlibro.component.css']
})
export class FormlibroComponent implements OnInit {
  //libroForm: any;
  id: any;
  listaGeneros: Genero[] = [];
  listaRestriccion: any[] = [];
  //generos: Genero[] = []; // [new Genero("Terror",1), new Genero("Comedia",2), new Genero("Policial",3)];
  url = "../../assets/img/metamorfosisPortada.jpg";
  formulario: any;
  caracteresNombre: number = 200;
  caracteresDetalle: number = 500;
  isArchivoCargado = false;
  formatoArchivo: string ='';

  constructor(private location: Location,
    private formBuilder: FormBuilder,
    private router: Router,
    private seccion: SeccionService,
    private escritorService: EscritorService
  ) { }

  ngOnInit() {
    this.cargarGeneros();
    this.cargarClasificacion();
    this.initForm();

    /*this.libroForm = this.formbuilder.group({
      titulo: ['', Validators.required],
      sipnosis: [''],
      genero: ['', Validators.required],
      clasificacion: ['', Validators.required],
      idioma: ['', Validators.required],
    })*/
  }

  initForm() {
    this.formulario = this.formBuilder.group({
      autor: [{ value: "1", disabled: true }, Validators.required],
      titulo: ['', Validators.required],
      sinopsis: ['', Validators.required],
      portada: ['', Validators.required],
      genero: ['', Validators.required],
      adulto: ['', Validators.required],
      exclusivo: [''],
      fechaPublicacion: ['', Validators.required]
    });

  }
  onSubmit(data: any) {
    console.log(data);
    /* this.libroService.sendLibro(data.titulo,data.sipnosis,data.genero,data.clasificacion,data.idioma).subscribe(data => {
       if(data){
         let res = JSON.parse(JSON.stringify(data));
         this.id=Number(res.id);
         console.log(res.id);
         this.seccion.send_seccion("Borrador",Number(this.id),"","0");
         this.router.navigate(["/vistaEscritura",this.id]);

       }
     })*/

    // console.log(this.id);
    // if(this.id!=""){
    //   this.seccion.send_seccion("Borrador",Number(this.id),"","1");
    // }
    this.formulario.reset();

  }

  atras() {
    this.location.back();
  }

  /* private cargarGeneros(){
     this.listaGeneros.push({id: 1, nombre:"Terror"});
     this.listaGeneros.push({id: 2, nombre:"Comedia"});
     this.listaGeneros.push({id: 3, nombre:"Novela"});
     this.listaGeneros.push({id: 4, nombre:"Poemas"});
   }*/


  private cargarGeneros() {
    this.escritorService.getTiposGeneros({}).subscribe(
      (respuesta: any) => {
        if (respuesta.error == null) {
          this.listaGeneros = respuesta.objeto;
        }
      },
      (error: HttpErrorResponse) => {
        //console.log("ingresooooooooooooooo" + error.status );
        //if (error.status == 404) {
        if (error.status == 0) {
          this.listaGeneros.push({ id: 1, nombre: "Terror" });
          this.listaGeneros.push({ id: 2, nombre: "Comedia" });
          this.listaGeneros.push({ id: 3, nombre: "Novela" });
          this.listaGeneros.push({ id: 4, nombre: "Poemas" });

        }
      },
      () => {
        this.listaGeneros.push({ id: 0, nombre: "Escoja un Generos" });
      });

  }//,
  /*(error: HttpErrorResponse) => {
    if (error.status == 401) this.authService.sesionCaducada();
  },
  () => {
    this.authService.actualizarDatosSesion();
  }
);
}*/
  private cargarClasificacion() {
    this.listaRestriccion.push({ id: true, nombre: WS.ES_ADULTO });
    this.listaRestriccion.push({ id: false, nombre: WS.ES_MENOR });
  }

  calcularCaracteres(any: any) {
    if (any.name == 'nombre') {
      this.caracteresNombre = 200 - any.value.length;
    }
    else if (any.name == 'sinopsis') {
      this.caracteresDetalle = 500 - any.value.length;
    }
  }

  onReiniciarFormulario(){
    this.formulario.reset();
		this.initForm();
    this.isArchivoCargado= false;
  }


  
	onCargarArchivo(inputFile: any) {
		const cargaArchivo = inputFile.target.files[0];
    
	}



}
