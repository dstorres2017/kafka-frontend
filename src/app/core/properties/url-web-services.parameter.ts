import { environment } from 'src/environments/environment';

const URL = environment.API_URL;


//  Obtener Categorias
export const GET_TIPOS_GENEROS = URL + 'escritor/get-generos';
