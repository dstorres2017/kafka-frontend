
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as WS from '../properties/url-web-services.parameter';

@Injectable({
    providedIn: 'root'
  })
export class EscritorService {

    constructor(private http: HttpClient) { }


    public getTiposGeneros(tipoGenero: { idtipoGenero?: number, estado?: string }) {
		return this.http.post(WS.GET_TIPOS_GENEROS, tipoGenero);
	}

}